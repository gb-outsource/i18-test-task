import { i18n } from '../src/i18n'

describe("i18n", () => {
    describe("'i18n' call", () => {
        it("'i18n'is function", () => {
            expect(i18n).toBeInstanceOf(Function)
        })
        it("'i18n' should not throw an error", () => {
            expect(() => i18n({ })).not.toThrow()
            expect(() => i18n({ })).not.toThrowError()
        })
    })
    describe("result", () => {
        it("'i18n' should return key as format-string function", () => {
            const t = i18n({ name: "Hello World" })
            expect(t.name).toBeInstanceOf(Function)
        })
        it("'i18n' must return formatted string", () => {
            const t: any = i18n({ name: "Hello, {username}" })
            expect(t.name({ username: "User" })).toBe("Hello, User")
        })
        it("'i18n' must return nested object", () => {
            const t: any = i18n({ user: { name: 'Hello, {name}' } })
            expect(t.user).toBeInstanceOf(Object)
        })
        it("'i18n' must return nested object", () => {
            const t: any = i18n({ user: { name: 'Hello, {name}' } })
            expect(t.user).toBeInstanceOf(Object)
        })
        it("'i18n' must return nested object with function", () => {
            const t: any = i18n({ user: { name: 'Hello, {name}' } })
            expect(t.user.name).toBeInstanceOf(Function)
        })
        it("'i18n' must return nested object with function, which can be called", () => {
            const t: any = i18n({ user: { name: 'Hello, {name}' } })
            const result = t.user.name({ name: "UserName" })
            expect(result).toBe('Hello, UserName')
        })
    })
})