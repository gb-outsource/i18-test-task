type FormatParams = { [key: string]: string | number }

type Result<R> = { 
    [K in keyof R]: R[K] extends string ? (params?: FormatParams) => string : Result<R[K]>
}

export function i18n<T extends Object>(namespaces: T): Result<T> {
    const result: any = {}

    function formatString(str: string) {
        return (params?: FormatParams) => {
            let result = str
            for(const [key, value] of Object.entries(params || {})) {
                result = result.replace(`{${key}}`, `${value}`)
            }
            return result
        }
    }

    function formatObject<T>(obj: T): Result<T> {
        const result: any = {}
        for(const key in obj) {
            const value = obj[key]
            if (value instanceof Object) {
                result[key] = formatObject(value) 
            } 
            if (typeof value === "string") {
                result[key] = formatString(value)
            }
        }
        return result as Result<T>
    }

    for(const key in namespaces) {
        const value = namespaces[key]
        if (value instanceof Object) {
            result[key] = formatObject<typeof value>(value)
        } 
        if (typeof value === "string") {
            result[key] = formatString(value)
        }
    }
    return result
}