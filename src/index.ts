import { i18n } from './i18n'

const sourceStrings = {
    hello: 'Добрый вечор, {username}!',
    admin: {
      objectForm: {
        label: 'Пароль администратора',
        hint: 'Не менее {minLength} символов. Сейчас ― {length}',
      },
    },
};

const t = i18n(sourceStrings);

console.log(t.hello({ username: "Hello" }))

console.log(t.admin.objectForm.hint({ minLength: 43, length: 2 }))